/**
 * Created by Phanton II on 17/01/2017.
 */
angular.module('starter.services')

    .service('$cart', ['$localStorage',function ($localStorage) {

        var key = 'cart';
        var cartAux = $localStorage.getObject(key);


        if (!cartAux){
            initCart();
        }

        this.clear = function () {

           initCart();

        };
        
        this.get = function () {

            //retorna todos os items do carrinho
            return $localStorage.getObject(key);

        };
        
        this.getItem = function (i) {

            //retorna um unico item do carrinho
            return this.get().items[i];

        };

        this.addItem = function (item) {
            var cart = this.get(), itemAux, exists = false;

            for (var index in cart.items) {
                itemAux = cart.items[index];
                if (itemAux.id == item.id) {
                    itemAux.qtd = item.qtd + itemAux.qtd;
                    itemAux.subtotal = calculateSubTotal(itemAux);
                    exists = true;
                    break;
                }
            }

            if (!exists) {
                item.subtotal = calculateSubTotal(item);
                cart.items.push(item);
            }

            cart.total = getTotal(cart.items);
            $localStorage.setObject(key, cart);
        };
        
        this.removeItem = function (i) {

            var cart = this.get();

            car.item.splice(i, 1); //remove um item por vez que é chamado
            cart.total = getTotal(cart.items);
            $localStorage.setObject(key, cart); //atualiza os dados do local storage
        }

        function calculateSubTotal(item) {
            return item.price * item.qtd;
        }

        function getTotal(items) {
            //metodo de atualizar o subtotal
            var sum = 0;

            angular.forEach(items, function (item) {

                //usa o foreach do agular para varrer o array de items (todos) e pega apenas um item
                //incremente esse subtotal calculado a variavel sum, que sera retornada e utilizada pelo metodo
                //para atualizar os dados referentes a subtotal no carrinho

                sum += item.subtotal;


            })

            return sum;
        }
        
        
        function initCart() {
            $localStorage.setObject(key, {
                items: [],
                total: 0
            })
        }

    }]);