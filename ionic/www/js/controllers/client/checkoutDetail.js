/**
 * Created by Phanton II on 12/01/2017.
 */

angular.module('starter.controllers')

    .controller('ClientCheckoutDetailCtrl', ['$scope', 'OAuth', '$ionicPopup', '$state', function ($scope, OAuth, $ionicPopup, $state) {

        console.log("Estamos aqui");
        $scope.user = {
            username: '',
            password: ''
        };


        $scope.login = function () {
            OAuth.getAccessToken($scope.user).then(function (data) {
                $ionicPopup.alert({
                    title: 'Bemvindo!',
                    template: 'Seja bem vindo ao nosso delivery <3'
                });
                $state.go('home')
            }, function (responseError) {
                $ionicPopup.alert({
                    title: 'ATENÇÃO!',
                    template: 'Login e/ou senha invalidos!.'
                });
            });
        }

    }]);