/**
 * Created by Phanton II on 12/01/2017.
 */

angular.module('starter.controllers')

    .controller('ClientViewProductsCtrl', [
        '$scope', '$state', 'Product', '$ionicLoading','$cart',
        function ($scope, $state, Product, $ionicLoading, $cart) {

        //delete window.localStorage['cart'];

        $scope.products = [];

        $ionicLoading.show({
            template: 'Carregando...'
        });

        Product.query({}, function (data) {
            $scope.products = data.data;
            $ionicLoading.hide();
        }, function (dataError) {
            $ionicLoading.hide();
        });

        $scope.addItem = function(item){

            item.qtd = 1; //define que a quantidade padrão a ser inserida por vez é 1

            $cart.addItem(item);//adiciona esse item no array de items do carrinho

            $state.go('client.checkout'); //redireciona o usuario para a view de checkout
        }



    }]);