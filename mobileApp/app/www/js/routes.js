angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('tabsController.pizzariaIsaac', {
    url: '/home',
    views: {
      'tab1': {
        templateUrl: 'templates/pizzariaIsaac.html',
        controller: 'pizzariaIsaacCtrl'
      }
    }
  })

  .state('meuCarrinho', {
    url: '/meuCarrinho',
    templateUrl: 'templates/meuCarrinho.html',
    controller: 'meuCarrinhoCtrl'
  })

  .state('tabsController.saibaComoChegar', {
    url: '/location',
    views: {
      'tab3': {
        templateUrl: 'templates/saibaComoChegar.html',
        controller: 'saibaComoChegarCtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('tabsController.extras', {
    url: '/extras',
    views: {
      'tab1': {
        templateUrl: 'templates/extras.html',
        controller: 'extrasCtrl'
      }
    }
  })

  .state('meusDados', {
    url: '/meus-dados',
    templateUrl: 'templates/meusDados.html',
    controller: 'meusDadosCtrl'
  })

  .state('tabsController.bebidas', {
    url: '/drinks',
    views: {
      'tab2': {
        templateUrl: 'templates/bebidas.html',
        controller: 'bebidasCtrl'
      }
    }
  })

  .state('historicoDePedidos', {
    url: '/historico-de-pedidos',
    templateUrl: 'templates/historicoDePedidos.html',
    controller: 'historicoDePedidosCtrl'
  })

  .state('caradpio', {
    url: '/caradapio',
    templateUrl: 'templates/caradpio.html',
    controller: 'caradpioCtrl'
  })

$urlRouterProvider.otherwise('/page1/home')

  

});