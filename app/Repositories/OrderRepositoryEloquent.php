<?php

namespace CodeDelivery\Repositories;

use CodeDelivery\Presenters\OrderPresenter;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Models\Order;
use CodeDelivery\Validators\OrderValidator;
use Prettus\Repository\Presenter\ModelFractalPresenter;

/**
 * Class OrderRepositoryEloquent
 * @package namespace CodeDelivery\Repositories;
 */
class OrderRepositoryEloquent extends BaseRepository implements OrderRepository
{
    /**
     * Specify Model class name
     *
     * @param $id
     * @param $idDeliveryman
     * @return string O metodo  gettByIdAndDeliveryman faz uma pesuisa pra encontrar o id do entregador
     *
     * O metodo  gettByIdAndDeliveryman faz uma pesuisa pra encontrar o id do entregador
     * para evitar que ele passe o id pela url e obtnha os dados de outro entregador
     *
     * o if serve para verificar se os dados reornados são uma collection
     * dai ele pega apenas o primeiro registro com o uso do metodo first()
     *
     * o metodo with() possui um array que define quais dados seram carregados quando o metodo for chamado
     * Ele define os relacionamentos. Ou seja, pega dados de outra tabela e insere no mesmo resultado (observando o
     * relacionamento entre as mesmas)
     */

    protected $skipPresenter = true;

    public function gettByIdAndDeliveryman($id, $idDeliveryman)
    {
        $result = $this->with(['client', 'items', 'cupom'])->findWhere([
            'id' => $id,
            'user_deliveryman_id' => $idDeliveryman
        ]);

        if ($result instanceof Collection) {
            $result = $result->first();
        } else{
            if (isset($result['data']) && count($result['data']) == 1){
                $result = [
                  'data' => $result['data'][0]
                ];
            } else{
                throw new ModelNotFoundException("Não existe pedido com o ID informado!");
            }
        }

//            if ($result){
//                //retorna os dados referentes ao produto
//                $result->items->each(function ($item){
//                    $item->product;
//                });
//            }

        //}

        return $result;
    }

    public function model()
    {
        return Order::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function presenter()
    {
        //return ModelFractalPresenter::class;
        return OrderPresenter::class;
    }





}
