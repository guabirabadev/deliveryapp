<?php
/**
 * Created by PhpStorm.
 * User: Phanton II
 * Date: 31/12/2016
 * Time: 15:27
 */

namespace CodeDelivery\Services;


use CodeDelivery\Models\Order;
use CodeDelivery\Repositories\CupomRepository;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\ProductRepository;
use Illuminate\Support\Facades\DB;

class OrderService
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var CupomRepository
     */
    private $cupomRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(
        OrderRepository $orderRepository,
        CupomRepository $cupomRepository,
        ProductRepository $productRepository
    )
    {

        $this->orderRepository = $orderRepository;
        $this->cupomRepository = $cupomRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @create
     * 1) recebe um array com todos os dados que são passados pela requisição
     * 2) define o status do pedido como sendo 0 (pendente)
     * 3) verifica da existencia de um cupom
     * 4
     * 5)Salva os dados ja alterados no array $data
     * 6) pesquisa o preço e a quantidade, bem como calcula o total
     * 7) adiciona o total a ordem
     * 8) salva a picareta no db
     */
    public function create(array $data)
    {

        DB::beginTransaction();
        try{
            $data['status'] = 0;

            if (isset($data['cupom_id'])){
                unset($data['cupom_id']);
            }

            if (isset($data['cupom_code'])){
                $cupom = $this->cupomRepository->findByField('code', $data['cupom_code'])->first();

                $data['cupom_id'] = $cupom->id;

                $cupom->used = 1;

                $cupom->save();

                unset($data['cupom_code']);
            }

            $items = $data['items'];
            unset($data['items']);

            $order = $this->orderRepository->create($data);

            $total = 0;
            foreach ($items as $item) {
                $item['price'] = $this->productRepository->find($item['product_id'])->price;

                $order->items()->create($item);

                $total += $item['price'] * $item['qtd'];
            }

            $order->total = $total;
            if (isset($cupom)){
                $order->total = $total - $cupom->value;
            }

            $order->save();

            \DB::commit();
            return $order;
        } catch (\Exception $exception){
          \DB::rollback();

            throw $exception;
        }

    }

    public function updateStatus($id, $idDeliveryman, $status)
    {
        $order = $this->orderRepository->gettByIdAndDeliveryman($id, $idDeliveryman);

        if ($order instanceof Order){
            $order->status = $status;
            $order->save();
            return $order;
        }

        return false;
    }
}