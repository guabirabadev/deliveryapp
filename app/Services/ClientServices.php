<?php
/**
 * Created by PhpStorm.
 * User: Phanton II
 * Date: 30/12/2016
 * Time: 01:25
 */

namespace CodeDelivery\Services;


use CodeDelivery\Repositories\ClientRepository;
use CodeDelivery\Repositories\UserRepository;

class ClientServices
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(ClientRepository $clientRepository, UserRepository $userRepository)
    {

        $this->clientRepository = $clientRepository;
        $this->userRepository = $userRepository;
    }

    public function update(array $data, $id)
    {
        $this->clientRepository->update($data, $id);

        $userId = $this->clientRepository->find($id, ['user_id'])->user_id; //pega o id que é passado e da
        //um buscar no user_id (na sua respectiva tabela

        //dd($userId);

       $this->userRepository->update($data['user'], $userId);
    }

    public function create(array $data)
    {
        $data['user']['password'] = bcrypt(123456); //define uma senha padrão para o usuario

        $user = $this->userRepository->create($data['user']);//pega um id pro novo usuario que esta sendo cadastrado
        //dd($user);

        $data['user_id'] = $user->id; //define esse userId no array
          //dd($data['user_id']);

        $this->clientRepository->create($data);//manda o dado pro db




    }
}