<?php

namespace CodeDelivery\Http\Controllers\Api\Client;

use CodeDelivery\Http\Controllers\Controller;
use CodeDelivery\Http\Requests\CheckoutRequest;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;


class ClientProductController extends Controller
{
    private $repository;

//    private $userRepository;
//
//    private $productRepository;
//
//    private $service;
//
//    private $with = [ 'client', 'cupom', 'items' ];

    public function __construct(  ProductRepository $repository
    )
    {

        $this->repository = $repository;

    }

    public function index()
    {
        $products = $this->repository->skipPresenter(false)->all();

        return $products;
    }





}
