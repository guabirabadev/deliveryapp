<?php

namespace CodeDelivery\Http\Controllers\Api\Client;

use CodeDelivery\Http\Controllers\Controller;
use CodeDelivery\Http\Requests\CheckoutRequest;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;


class ClientCheckoutController extends Controller
{
    private $repository;

    private $userRepository;

    private $productRepository;

    private $service;

    private $with = [ 'client', 'cupom', 'items' ];

    public function __construct(
        OrderRepository $repository,
        UserRepository $userRepository,
        ProductRepository $productRepository,
        OrderService $service
    )
    {

        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->service = $service;
    }

    public function index()
    {
        $id = Authorizer::getResourceOwnerId();

        $clientId = $this->userRepository->find($id)->client->id;

        $orders = $this->repository
            ->skipPresenter(false)
            ->with($this->with)
            ->scopeQuery(function ($query) use ($clientId){
           return $query->where('client_id', '=', $clientId);
        })->paginate();

        return $orders;
    }


    public function store(CheckoutRequest $request)
    {
        $data = $request->all();

        $id = Authorizer::getResourceOwnerId();

        $clientId = $this->userRepository->find($id)->client->id;

        //$clientId = $this->userRepository->find(Auth::user()->id)->client->id;

        $data['client_id'] = $clientId;

        $o = $this->service->create($data);
        //dd($o);
        //$o = $this->repository->with('items')->find($o->id);  //retorna os items da ordem
        return $this->repository
                                ->skipPresenter(false)
                                ->with($this->with)
                                ->find($o->id);


        //return $o;
    }

    public function show($id)
    {                       //skipPresenter()->
       // $o = $this->repository->with(['client', 'items', 'cupom'])->find($id);

//        $o->items->each(function ($item){
//            $item->product;
//        });

        //return $o;

        return $this->repository->skipPresenter(false)->with($this->with)->find($id);
    }

}
