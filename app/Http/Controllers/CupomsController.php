<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminCategoryRequest;
use CodeDelivery\Http\Requests\AdminCupomRequest;
use CodeDelivery\Repositories\CategoryRepository;
use CodeDelivery\Repositories\CupomRepository;
use Illuminate\Http\Request;

use CodeDelivery\Http\Requests;
use CodeDelivery\Http\Controllers\Controller;

class CupomsController extends Controller
{
    private $repository;

    public function __construct(CupomRepository $repository)
    {

        $this->repository = $repository;
    }

    public function index()
    {
        $cupoms = $this->repository->paginate(5);
        return view('admin.cupoms.index', compact('cupoms'));
    }

    public function create()
    {
        return view('admin.cupoms.create');
    }

    public function store(AdminCupomRequest $request)
    {
        $data = $request->all(); //pega todos os dados que são enviados pelo request no caso  o nome da categoria que vem do fomulario
        $this->repository->create($data);    //manda pro repository salvar no db

        return redirect()->route('admin.cupoms.index'); //redireciona pra pagina inicial
    }

    public function edit($id)
    {
        //o metodo edit recebe um id vindo da ulr
        $category = $this->repository->find($id); //ele pesquisa por esse id no repository
        return view('admin.categories.edit', compact('category')); //retorna essa requisição pra view

    }

    public function update(AdminCategoryRequest $request, $id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);

        return redirect()->route('admin.categories.index');
    }
}
