<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminCategoryRequest;

use CodeDelivery\Http\Requests\AdminProductRequest;
use CodeDelivery\Http\Requests\AdminProductRequestRequest;
use CodeDelivery\Repositories\CategoryRepository;
use CodeDelivery\Repositories\ProductRepository;
use Illuminate\Http\Request;

use CodeDelivery\Http\Requests;
use CodeDelivery\Http\Controllers\Controller;

class ProductsController extends Controller
{
    private $repository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(ProductRepository $repository, CategoryRepository $categoryRepository)
    {

        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $products = $this->repository->paginate(5);
        //dd($products);
        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        $categories = $this->categoryRepository->lists(['name', 'id']);
        return view('admin.products.create', compact('categories'));
    }

    //public function store(AdminCategoryRequest $request)
    public function store(AdminProductRequest $request)
    {
        $data = $request->all(); //pega todos os dados que são enviados pelo request no caso  o nome da categoria que vem do fomulario
        $this->repository->create($data);    //manda pro repository salvar no db

        return redirect()->route('admin.products.index'); //redireciona pra pagina inicial
    }

    public function edit($id)
    {
        //o metodo edit recebe um id vindo da ulr
        $product = $this->repository->find($id); //ele pesquisa por esse id no repository

        $categories = $this->categoryRepository->lists(['name', 'id'])   ;

        return view('admin.products.edit', compact('product', 'categories')); //retorna essa requisição pra view

    }

    public function update(AdminProductRequest $request, $id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);

        return redirect()->route('admin.products.index');
    }

    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('admin.products.index');
    }
}
