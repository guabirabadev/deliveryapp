<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminCategoryRequest;
use CodeDelivery\Repositories\CategoryRepository;
use Illuminate\Http\Request;

use CodeDelivery\Http\Requests;
use CodeDelivery\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    private $repository;

    public function __construct(CategoryRepository $repository)
    {

        $this->repository = $repository;
    }

    public function index()
    {
        $categories = $this->repository->paginate(5);
        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(AdminCategoryRequest $request)
    {
        $data = $request->all(); //pega todos os dados que são enviados pelo request no caso  o nome da categoria que vem do fomulario
        $this->repository->create($data);    //manda pro repository salvar no db

        return redirect()->route('admin.categories.index'); //redireciona pra pagina inicial
    }

    public function edit($id)
    {
        //o metodo edit recebe um id vindo da ulr
        $category = $this->repository->find($id); //ele pesquisa por esse id no repository
        return view('admin.categories.edit', compact('category')); //retorna essa requisição pra view

    }

    public function update(AdminCategoryRequest $request, $id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);

        return redirect()->route('admin.categories.index');
    }
}
