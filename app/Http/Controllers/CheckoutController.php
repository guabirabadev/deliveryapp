<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\CheckoutRequest;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CheckoutController extends Controller
{
    private $repository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var OrderService
     */
    private $service;

    public function __construct(
        OrderRepository $repository,
        UserRepository $userRepository,
        ProductRepository $productRepository,
        OrderService $service
    )
    {

        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->service = $service;
    }

    public function index()
    {
        $clientId = $this->userRepository->find(Auth::user()->id)->client->id;

        $orders = $this->repository->scopeQuery(function ($query) use ($clientId){
           return $query->where('client_id', '=', $clientId);
        })->paginate(5);
        return view('customer.order.index', compact('orders'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * Quando se passa o method lists, e se define em seu metodo contsutor
     * os dados que devem ser passados como parametro, deve-se especificar
     * exatamente os campos que queremos recuperar:
     *   $products = $this->productRepository->lists('id', 'name');
     * pois la na sua definição, definiu-se um array que ira conter tais dados.
     *
     */
    public function create()
    {
        $products = $this->productRepository->lists('id', 'name', 'price');

        return view('customer.order.create', compact('products'));
    }

    public function store(CheckoutRequest $request)
    {
        $data = $request->all();

        $clientId = $this->userRepository->find(Auth::user()->id)->client->id;

        $data['client_id'] = $clientId;

        $this->service->create($data);

        return redirect()->route('customer.order.index');
    }

}
