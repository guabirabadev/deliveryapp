<?php

namespace CodeDelivery\Http\Middleware;

use Closure;
use CodeDelivery\Repositories\UserRepository;


use LucaDegasperi\OAuth2Server\Facades\Authorizer;


class OAuthCheckRole
{

    private $userRepository;


    public function __construct(UserRepository $userRepository)
    {

        $this->userRepository = $userRepository;
    }

    public function handle($request, Closure $next, $role)
    {
       $id = Authorizer::getResourceOwnerId();   //pega o usuario autentticado

        $user = $this->userRepository->find($id); //verifica se o usuario existe
        //dd($user);

        if ($user->role != $role){  //verifica se a role do usuario lhe permite acesso
            abort(403, 'Acesso não autorizado');
        }

        return $next($request);
    }
}
