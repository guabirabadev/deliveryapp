<?php

namespace CodeDelivery\Http\Requests;

use Illuminate\Http\Request as HttpRequest;


/**
 * Class CheckoutRequest
 * @package CodeDelivery\Http\Requests
 */
class CheckoutRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param HttpRequest $request
     * @return array essa validação que é passada faz o seguinte:
     *
     * essa validação que é passada faz o seguinte:
     * verifica se existe um cupom  na tabela do DB (exists:cupoms,code) -
     * o code se refere ao ID do cupom de desconto
     * e verifica ainda se esse cupom esta usado ou não ( used,0)
     */
    public function rules(HttpRequest $request)
    {
        $rules = [
            'items.0.product_id' => 'required',
            'cupom_code' => 'exists:cupoms,code,used,0'
        ];

        $this->buildRulesItems(0, $rules);   //instancia a validação de items

        $items = $request->get('items', []); //verifica se os itens foram passados, se não ele peg aum array vazio

        $items = !is_array($items) ? [] : $items; //verifica se os dados recebidos são realmente um array, se não for transofrma em um
        //elimina o erro de "Undefined index: items"

        foreach ($items as $key => $val){  //faz as varreduras das regras
            $this->buildRulesItems($key, $rules);
        }
//        return [
//
//            'cupom_code' => 'exists:cupoms,code,used,0',
//            'items.0.product_id' => 'required',
//            'items.0.qtd' => 'required'
//        ];

        return $rules;

    }

    /**
     * @param $key
     * @param array $rules
     *
     * Cria a validação dos dados passados (items: produto e quantidade)
     */
    public function buildRulesItems($key, array &$rules)
    {
        $rules ["items.$key.product_id"]  = 'required';
        $rules ["items.$key.qtd"]  = 'required';

    }
}
