<?php

namespace CodeDelivery\Transformers;

use League\Fractal\TransformerAbstract;
use CodeDelivery\Models\Order;

/**
 * Class OrderTransformer
 * @package namespace CodeDelivery\Transformers;
 */
class OrderTransformer extends TransformerAbstract
{
    //defautlIncludes serializam dados que são orbigatorios de serem carregados
     //protected $defaultIncludes = ['cupom', 'items'];
     protected $availableIncludes = ['cupom', 'items', 'client'];
    //availableIncludes so passam dados pra serializar quando são requisitados
     //protected $availableIncludes = [];
    /**
     * Transform the \Order entity
     * @param \Order $model
     *
     * @return array
     */
    public function transform(Order $model)
    {
        return [
            'id'         => (int) $model->id,
            'total' => (float) $model->total,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }

    public function includeClient(Order $model)
    {
       return $this->item($model->client, new ClientTransformer());
    }


    public function includeCupom(Order $model)
    {
        //modle pra serialziar relacionamentos

          if (!$model->cupom){
              return null;
          }
        return $this->item($model->cupom, new CupomTransformer());
    }

    public function includeItems(Order $model)
    {
        return $this->collection($model->items, new OrderItemTransformer());
    }
}
