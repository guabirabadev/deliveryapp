@extends('app')


@section('content')

    <div class="container">
        <h3>Editar categoria {{ $category->name }}</h3>

            @include('errors._check')

            <!-- Passando esse array de rotas, da pra passar o action do form diretamente sem ter que
            definir um arquivo .php especifico para ser o action, ou seja, basta setar o nome de uma rota que ele ja vai
            usar a mesma-->

            {!! Form::model($category,  ['route' => ['admin.categories.update', $category->id]]) !!}

               @include('admin.categories._form')

                    <div class="form-group">


                    	{!! Form::submit( 'Salvar alterações', ['class' => 'btn btn-primary']) !!}
                    </div>

            {!! Form::close() !!}

    </div>

@endsection