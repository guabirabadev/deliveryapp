@extends('app')


@section('content')

    <div class="container">
        <h3>Cupons</h3>

        <a href="{{ route('admin.cupoms.create') }}" class="btn btn-success">Novo cupom</a>
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Id</th>
                <th>Codigo</th>
                <th>Valor</th>
                <th>Ação</th>
            </tr>
            </thead>

            <tbody>
            @foreach($cupoms as $cupom)
            <tr>
                <td>{{ $cupom->id }}</td>
                <td>{{ $cupom->code }}</td>
                <td>{{ $cupom->value }}</td>
                {{--<td>--}}
                    {{--<a href="{{ route('admin.cupoms.edit', ['id' => $cupom->id]) }}" class="btn btn-danger btn-sm">Editar</a>--}}
                {{--</td>--}}

            </tr>
            @endforeach
            </tbody>
        </table>

        {!! $cupoms->render() !!}

    </div>

@endsection