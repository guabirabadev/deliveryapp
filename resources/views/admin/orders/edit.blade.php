@extends('app')

@section('content')
    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading">Pedidos #{{ $order->id }} - R$ {{ $order->total }}</div>
            <div class="panel-body">
                <h3>Cliente: <b> {{ $order->client->user->name }}</b></h3>
                <h4>Data: {{ $order->created_at }}</h4>


                    <p>Entregar em: <br>
                        {{ $order->client->address }}   - {{ $order->client->city }} - {{ $order->client->state }}
                    </p>


                {!! Form::model($order, ['route' => ['admin.orders.update', $order->id], 'class' => 'form-inline'])  !!}

                    <div class="form-group">
                    	{!! Form::label('Status', 'Status:') !!}

                    	{!! Form::select('status', $list_status, null ,['class' => 'form-control']) !!}
                    </div>

                <div class="form-group">
                    {!! Form::label('Entregador', 'Entregador:') !!}

                    {!! Form::select('user_deliveryman_id', $deliveryman, null ,['class' => 'form-control']) !!}
                </div>

                    <div class="form-group">
                        {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
                    </div>
                {!! Form::close() !!}


            </div>
        </div>

    </div>
@endsection